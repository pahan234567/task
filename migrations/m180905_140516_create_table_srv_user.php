<?php

use yii\db\Migration;

/**
 * Class m180905_140516_create_table_srv_user
 */
class m180905_140516_create_table_srv_user extends Migration
{
    public function up()
    {
        $this->createTable('srv_user', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable('srv_user');
    }
}
