<?php

use yii\db\Migration;

/**
 * Class m180906_074025_add_columns_to_user
 */
class m180906_074025_add_columns_to_user extends Migration
{
    public function up()
    {
      $this->addColumn('srv_user','first_name', $this->string(50));
      $this->addColumn('srv_user','middle_name', $this->string(50));
      $this->addColumn('srv_user','last_name', $this->string(50));
      $this->addColumn('srv_user','img', $this->string(50));
    }

    public function down()
    {
      $this->dropColumn('srv_user', 'first_name');
      $this->dropColumn('srv_user', 'middle_name');
      $this->dropColumn('srv_user', 'last_name');
      $this->dropColumn('srv_user', 'img_name');
    }
}
