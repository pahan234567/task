<?php

use yii\db\Migration;

/**
 * Class m180905_140910_create_table_srv_advert
 */
class m180905_140910_create_table_srv_advert extends Migration
{

    public function up()
    {
       $this->createTable('srv_advert', [
           'id' => $this->primaryKey(),
           'title' => $this->string(255)->notNull(),
           'description' => $this->text(),
           'cost' => $this->integer(),
           'status' => $this->integer(2),
           'user_id' => $this->integer(),
           'created_at' => $this->integer(),
           'updated_at' => $this->integer()
       ]);

       $this->createIndex('idx-srv_advert-user_id', 'srv_advert','user_id');

       $this->addForeignKey('fk-srv_advert-user_id', 'srv_advert','user_id',
           'srv_user',
           'id',
           'CASCADE');
    }

    public function down()
    {
      $this->dropIndex('idx-srv_advert-user_id', 'srv_advert');

      $this->dropForeignKey('fk-srv_advert-user_id', 'srv_advert');

      $this->dropTable('srv_advert');
    }
}
