<?php
$this->title = 'Доска объявлений';

use yii\helpers\Html;
?>

    <?php foreach ($adverts as $advert): ?>
    <!-- Post -->
    <article class="post">
        <header>
            <div class="title">
                <h2><?= Html::a($advert->title,['site/view', 'id' => $advert->id]) ?></h2>
                <p>Статус: <b><?= $advert->getStatus() ?></b></p>
            </div>
            <div class="meta">
                <time class="published" datetime="2015-11-01"><?= Yii::$app->formatter->asDatetime($advert->created_at) ?></time>
                <a href="/user/view/?id=<?= $advert->user->id ?>" class="author"><span class="name"><?= $advert->user->initials ?></span><?= Html::img($advert->user->avatar) ?></a>
            </div>
        </header>
        <img style="width:20%" src="images/pic01.jpg" alt="" />
        <p><?= Html::encode($advert->description) ?></p>
        <footer>
            <ul class="actions">
                <li><?= Html::a('Продолжить читать',['site/view', 'id' => $advert->id],['class' => 'button big']) ?></li>
            </ul>
            <ul class="stats">
                <li><h1>Цена: <?= $advert->cost ?> руб.</h1></li>
            </ul>
        </footer>
    </article>

<?php endforeach; ?>

<?= \yii\widgets\LinkPager::widget(['pagination' => $pagination]) ?>
