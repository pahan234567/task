<?php

$this->title = $advert->title . ' - цена: ' . $advert->cost . ' руб.';

use yii\helpers\Html;
?>

<!-- Post -->
<article class="post">
    <header>
        <div class="title">
            <h2><a href="#"><?= Html::encode($advert->title) ?></a></h2>
            <p>Статус: <b><?= $advert->getStatus() ?></b></p>
        </div>
        <div class="meta">
            <time class="published" datetime="2015-11-01"><?= Yii::$app->formatter->asDatetime($advert->created_at) ?></time>
            <a href="/user/view/?id=<?= $advert->user->id ?>" class="author"><span class="name"><?= $advert->user->initials ?></span><?= Html::img($advert->user->avatar) ?></a>
        </div>
    </header>
    <a href="#" class="image featured"><img style="width:20%" src="/images/pic01.jpg" alt="" /></a>
    <p><?= Html::encode($advert->description) ?></p>
    <footer>
        <ul class="actions">
            <?= Html::a('Купить сейчас', ['site/view', 'id' => $advert->id], [
                'class' => 'btn btn-primary',
                'data' => [
                    'confirm' => 'Вы действительно желаете купить?',
                    'method' => 'post',
                ],
            ]) ?>
        </ul>
        <ul class="stats">
            <li><h1>Цена: <?= $advert->cost ?> руб.</h1></li>
        </ul>
    </footer>
</article>