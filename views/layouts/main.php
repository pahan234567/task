<?php

use app\assets\AppAsset;
use yii\helpers\Html;

AppAsset::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE HTML>
<html lang="<?= Yii::$app->language ?>">
<head>
    <title><?= Html::encode($this->title) ?></title>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<!-- Wrapper -->
<div id="wrapper">

    <!-- Header -->
    <header id="header">
        <h1><a href="/">Доска объявлений</a></h1>
        <nav class="links">
            <ul>
                <li><?= Html::a('Объявления', '/') ?></li>

                <?php if (Yii::$app->user->isGuest): ?>
                    <li><?= Html::a('Регистрация', '/site/signup'); ?></li>
                    <li><?= Html::a('Войти', '/site/login'); ?></li>
                <?php endif; ?>

                <?php if (!Yii::$app->user->isGuest): ?>
                    <li><?= Html::a('Создать объявление', '/advert/create') ?></li>
                    <li><?= Html::a('Админ панель', '/advert') ?></li>
                    <li><?= Html::a('Мой профиль', ['/user/view' , 'id' => Yii::$app->user->id]) ?></li>
                    <li><?= Html::a('Пользователи', '/user') ?></li>
                    <li><?= Html::a('Выйти', '/site/logout'); ?></li>
                <?php endif; ?>

            </ul>
        </nav>
        <nav class="main">
            <ul>
                <li class="search">
                    <a class="fa-search" href="#search">Поиск</a>
                    <form id="search" method="get" action="#">
                        <input type="text" name="query" placeholder="Search" />
                    </form>
                </li>
            </ul>
        </nav>
    </header>

    <!-- Menu -->
    <section id="menu">

        <!-- Search -->
        <section>
            <form class="search" method="get" action="#">
                <input type="text" name="query" placeholder="Search" />
            </form>
        </section>

        <!-- Links -->
        <section>
            <ul class="links">
                <li>
                    <a href="#">
                        <h3>Lorem ipsum</h3>
                        <p>Feugiat tempus veroeros dolor</p>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <h3>Dolor sit amet</h3>
                        <p>Sed vitae justo condimentum</p>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <h3>Feugiat veroeros</h3>
                        <p>Phasellus sed ultricies mi congue</p>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <h3>Etiam sed consequat</h3>
                        <p>Porta lectus amet ultricies</p>
                    </a>
                </li>
            </ul>
        </section>

        <!-- Actions -->
        <section>
            <ul class="actions vertical">
                <li><a href="#" class="button big fit">Log In</a></li>
            </ul>
        </section>

    </section>

    <!-- Main -->
    <div id="main">
        <?= $content ?>
    </div>
    <!-- Sidebar -->
    <section id="sidebar">

        <!-- Intro -->
        <section id="intro">
            <a href="#" class="logo"><img src="images/logo.jpg" alt="" /></a>
            <header>
                <h2>Доска объявлений</h2>
                <p>Добавьте объявление сегодня, и получите список желающих приобрести товар уже завтра!</a></p>
            </header>
        </section>

        <!-- Posts List -->
        <section>
            <?php $adverts = \app\models\Advert::find()->orderBy('id DESC')->limit(5)->all(); ?>
            <ul class="posts">
                <?php foreach ($adverts as $advert): ?>
                <li>
                    <article>
                        <header>
                            <h3><?= Html::a($advert->title,['site/view', 'id' => $advert->id]) ?></h3>
                            <time class="published" datetime="2015-10-20"><?= Yii::$app->formatter->asDatetime($advert->created_at) ?></time>
                        </header>
                        <a href="/" class="image"><?= Html::img($advert->user->avatar) ?></a>
                    </article>
                </li>
                <?php endforeach; ?>
            </ul>
        </section>

        <!-- About -->
        <section class="blurb">
            <h2>О нас</h2>
            <p>Коммерческий проект, разработанный с целью автоматизации товарооборота.</p>
        </section>

        <!-- Footer -->
        <section id="footer">
            <ul class="icons">
                <li><a href="#" class="fa-twitter"><span class="label">Twitter</span></a></li>
                <li><a href="#" class="fa-facebook"><span class="label">Facebook</span></a></li>
                <li><a href="#" class="fa-instagram"><span class="label">Instagram</span></a></li>
                <li><a href="#" class="fa-rss"><span class="label">RSS</span></a></li>
                <li><a href="#" class="fa-envelope"><span class="label">Email</span></a></li>
            </ul>
            <p class="copyright">&copy; Untitled. Crafted: <a href="http://designscrazed.org/">HTML5</a>.</p>
        </section>

    </section>

</div>

<?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>